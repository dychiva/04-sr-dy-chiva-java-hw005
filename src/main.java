public class main extends Thread{
    public String message;

    //constructor with parameter
    main(String message){
        this.message = message;
    }

    @Override
    public void run() {
        for(int i=0; i< message.length();i++){
            System.out.print(message.charAt(i));
            try {
                Thread.sleep(300);
            }catch (Exception ex){}
        }
    }

    //main
    public static void main(String[] args) {
        main thread1 = new main("Hello KSHRD!");
        main thread2 = new main("**********************");
        main thread3 = new main("I will try my best to be here at HRD.");
        main thread4 = new main("----------------------");
        main thread5 = new main("..........");
        try{
            thread1.start();
            thread1.join();
            System.out.println();

            thread2.start();
            thread2.join();
            System.out.println();

            thread3.start();
            thread3.join();
            System.out.println();

            thread4.start();
            thread4.join();
            System.out.println();

            System.out.print("Downloading");
            thread5.start();
            thread5.join();
            System.out.print("Completed 100%!");
        }catch (Exception ex){}
    }
}
